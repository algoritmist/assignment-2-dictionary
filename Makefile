ASM=nasm
ASM_FLAGS=-g -felf64
LD=ld


build: clean out/program
run: build
	out/./program

build/%.o: src/%.asm src/lib.inc src/dict.inc src/colon.inc src/constants.inc
	$(ASM) $(ASM_FLAGS) -o $@ $<

out/program: build/main.o build/dict.o build/lib.o
	$(LD) -o $@ $^

clean:
	rm -rf build/*
	rm -rf out/*

.PHONY: build clean run


	
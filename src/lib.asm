%define stdin 0
%define stdout 1
%define syscall_read 0
%define syscall_write 1
%define syscall_exit 60

section .text

global exit
global string_length
global print_string
global print_uint
global print_int
global print_char
global print_newline
global string_equals
global string_copy
global read_char
global read_word
global parse_uint
global parse_int

; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax
    mov rax, syscall_exit
    xor rdi, rdi
    syscall

sys_write:
    mov rdi, stdout
    mov rax, syscall_write
    syscall
    ret

sys_read:
    mov rdi, stdin
    mov rax, syscall_read
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; (str*:rdi) -> rax
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        jz .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; (str*:rdi) -> void
print_string:
    call string_length
    mov  rsi, rdi
    mov  rdx, rax
    call sys_write
    ret

; Принимает код символа и выводит его в stdout
; (char:di) -> void
print_char:
    push di
    mov rsi, rsp; get top char
    mov rdx, 1; write length = 1
    call sys_write
    pop di
    ret

; Переводит строку (выводит символ с кодом 0xA)
; void -> void
print_newline:
    mov di, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды    
; (int64*:rdi) -> void
print_uint:
    mov rax, rdi; div(RAX:RDX)
    mov rdi, 10; base
    mov r8, rsp; save stack
    
    push word 0; place null-terminator at the bottom of the stack
    .loop:
        xor rdx, rdx
        div rdi
        or dl, "0"; add char code
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        jnz .loop
    .end:
        mov rdi, rsp
        call print_string
        mov rsp, r8; restore rsp
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
; (int64*:rdi) -> void 
print_int:
    cmp rdi, 0
    jge .print_uint
    .print_minus:
        push rdi
        mov di, "-"
        call print_char
        pop rdi
        neg rdi
    .print_uint:
        call print_uint
        ret 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; (str*:rdi, str*:rsi) -> bool
string_equals:
    xor rax, rax
    xor rcx, rcx
    .loop:
        mov dl, [rdi + rcx]
        cmp dl, [rsi + rcx]
        jne .end
        inc rcx
        test dl, dl
        jnz .loop
    inc al
    .end:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; void -> char
read_char:
    push word 0; char to read
    xor rax, rax
    mov rsi, rsp
    mov rdx, 1
    call sys_read
    pop ax; restores word
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; void -> (str*: rax, len: rdx)
read_word:
    push r12
    xor r12, r12; size counter
    mov r8, rdi; start
    mov r9, rsi; max-size
        
    .loop:
        call read_char
        cmp al, 0x20
        je .check_is_start
        cmp al, 0x9
        je .check_is_start
        cmp al, 0xA
        je .check_is_start
        
        test al, al
        jz .write_end
        
        ;cmp r12, r9
        ;je .overflow

        mov [r8 + r12], al
        inc r12
        jmp .loop

    .check_is_start:
        test r12, r12
        jz .loop
    .write_end:
        mov byte [r8 + r12], 0
    .end:
        cmp r12, r9
        ja .overflow; read the entire word to prevent unparsed bytes
        mov rax, r8
        mov rdx, r12
        pop r12
        ret
    .overflow:
        mov rax, 0
        pop r12
        ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; void -> (int64: rax, len: rdx)
parse_uint:
    xor	rax, rax
	xor	rsi, rsi; length
	xor	rcx, rcx; digit
	mov	r8, 10; base
    .loop:
	    mov	cl, byte [rdi + rsi]
	    xor	cl, "0";     to number
	    cmp	cl, 9
	    ja	.end
	    mul	r8;          mul(RDX:RAX)
	    add	rax, rcx;    add digit
	    inc	rsi
	    jmp	.loop
    .end:
	    mov	rdx, rsi
	    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; void -> (int64: rax, len: rdx)
parse_int:
    xor rax, rax
    cmp byte [rdi], "-"
    je .neg_int
    call parse_uint
    ret    
    .neg_int:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; (str*: rdi, buf*: rsi, buffer_len: rdx) -> rax 
string_copy:
    xor rax, rax; cur length
    xor rcx, rcx
    .loop:
        cmp rax, rdx
        jae .overflow
        mov cl, [rdi + rax]
        mov [rsi + rax], cl
        test cl, cl
        jz .end
        inc rax
        jmp .loop
    .overflow:
        xor rax, rax
    .end:
        ret
        
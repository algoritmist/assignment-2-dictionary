%include "src/lib.inc"
%include "src/constants.inc"

global find_word

section .text

; (src*: rdi, dict*: rsi) -> start*: rax
; returns the start of word
find_word:
    .loop:
        test rsi, rsi
        jz .break
        add rsi, PTR_SIZE; skip next-word
        push rdi
        push rsi
        call string_equals
        pop rsi
        pop rdi
        sub rsi, PTR_SIZE
        test rax, rax
        jnz .found
        mov rsi, [rsi]; get the address of next element
        jmp .loop
    .break:
        xor rax, rax
        ret
    .found:
        mov rax, rsi
        ret
        
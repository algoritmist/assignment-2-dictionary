%include "src/lib.inc"
%include "src/colon.inc"
%include "src/dict.inc"
%include "src/words.inc"
%include "src/constants.inc"

global _start

section .rodata:
    welcome: db "Welcome, lab2 solver at your service", 0
    prompt: db "Query: ", 0
    read_unsuccessful: db "Your input is too big", 0
    word_not_found: db "Sorry, no such word in dict", 0
    word_found: db "The value is: ", 0
    max_word_size: db "Max input length: ", 0
    max_word_size_symbols: db " symbols", 0

section .bss
    buffer: resb BUFFER_SIZE

section .text
_start:
    mov rdi, welcome
    call print_string
    call print_newline

    ; print max word size
    mov rdi, max_word_size
    call print_string
    mov rdi, BUFFER_SIZE
    call print_uint
    mov rdi, max_word_size_symbols
    call print_string
    call print_newline
    
    .read:
        mov rdi, prompt
        call print_string
        mov rdi, buffer
        mov rsi, BUFFER_SIZE
        call read_word
        test rax, rax
        jz .read_fault
        push rdx; size of word
    
    .find:
        mov rdi, rax
        mov rsi, list_start
        call find_word
        test rax, rax
        jz .find_fail

    .find_success:
        push rax; word start
        mov rdi, word_found
        call print_string
        pop rdi
        pop rdx
        add rdi, PTR_SIZE + 1
        add rdi, rdx
        call print_string
        call print_newline
        jmp .read
        
    .find_fail:
        mov rdi, word_not_found
        call print_string
        call print_newline
        add rsp, PTR_SIZE
        jmp .read
        
    .read_fault:
        mov rdi, read_unsuccessful
        call print_string
        call print_newline
        jmp .read
